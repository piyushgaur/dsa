public class App {
    public static void main(String[] args) throws Exception {
        ShortestPath o = new ShortestPath();
        int n = 6, m = 7;
        int[][] edges = { { 0, 1, 2 }, { 0, 4, 1 }, { 4, 5, 4 }, { 4, 2, 2 }, { 1, 2, 3 }, { 2, 3, 6 }, { 5, 3, 1 } };
        o.shortestPath(n, m, edges);
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

/**
 * Pair
 */

public class ShortestPath {
    class Pair {
        int node, dist;

        Pair(int n, int d) {
            node = n;
            dist = d;
        }

    }

    /**
     * Given a Directed Acyclic Graph of N vertices from 0 to N-1 and a 2D Integer
     * array(or vector) edges[ ][ ] of length M, where there is a directed edge from
     * edge[i][0] to edge[i][1] with a distance of edge[i][2] for all i, 0<=i
     * 
     * Find the shortest path from src(0) vertex to all the vertices and if it is
     * impossible to reach any vertex, then return -1 for that vertex.
     * 
     * @param N
     * @param M
     * @param edges
     * @return
     */

    public int[] shortestPath(int N, int M, int[][] edges) {
        int[] dist = new int[N];
        boolean[] vis = new boolean[N];
        Stack<Integer> st = new Stack<>();
        ArrayList<ArrayList<Pair>> adj = new ArrayList<>();
        int src = 0, u, v, d, x, y;

        Arrays.fill(dist, Integer.MAX_VALUE);

        // initialization
        for (int i = 0; i < N; i++) {
            adj.add(new ArrayList<>());
        }

        for (int i = 0; i < edges.length; i++) {
            u = edges[i][0];
            v = edges[i][1];
            d = edges[i][2];
            adj.get(u).add(new Pair(v, d));
        }

        dist[src] = 0;
        topoSort(adj, src, vis, st);

        while (!st.isEmpty()) {
            u = st.pop();
            for (Pair p : adj.get(u)) {
                v = p.node;
                d = p.dist;
                x = dist[u];
                y = dist[v];

                dist[v] = y < x + d ? y : x + d;
            }
        }

        for (int i = 0; i < N; i++) {
            dist[i] = dist[i] == Integer.MAX_VALUE ? -1 : dist[i];
        }

        return dist;
    }

    public void topoSort(ArrayList<ArrayList<Pair>> adj, int s, boolean[] vis, Stack<Integer> st) {
        vis[s] = true;

        for (Pair u : adj.get(s)) {
            if (!vis[u.node]) {
                topoSort(adj, u.node, vis, st);
            }
        }
        st.push(s);
    }
}
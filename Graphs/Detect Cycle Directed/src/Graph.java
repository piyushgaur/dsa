import java.util.ArrayList;

public class Graph {
    // int v;
    // ArrayList<ArrayList<Integer>> g;

    // Graph(int v) {
    // this.v = v;
    // g = new ArrayList<ArrayList<Integer>>(v);

    // for (int i = 0; i < v; i++) {
    // g.add(new ArrayList<Integer>());
    // }
    // }

    // public void addEdge(int s, int d) {
    // // directed graph
    // g.get(s).add(d);
    // }

    // public boolean detectCycle() {
    // boolean[] vis = new boolean[v];
    // boolean[] recst = new boolean[v];

    // for (int i = 0; i < v; i++) {
    // if (!vis[i] && dfs(i, vis, recst)) {
    // return true;
    // }
    // }
    // return false;
    // }

    // private boolean dfs(int s, boolean[] vis, boolean[] recst) {

    // vis[s] = true;
    // recst[s] = true;

    // for (Integer x : g.get(s)) {
    // if (!vis[x] && dfs(x, vis, recst)) {
    // return true;
    // } else if (recst[x]) {
    // // visited true and recst true
    // return true;
    // }
    // }
    // recst[s] = false;
    // return false;
    // }
    ArrayList<ArrayList<Integer>> g;
    int v;

    Graph(int V) {
        v = V;
        g = new ArrayList<>();

        // initialize
        for (int i = 0; i < v; i++) {
            g.add(new ArrayList<>());
        }
    }

    public void addEdge(int s, int d) {
        // directed graph
        g.get(s).add(d);
    }

    public boolean detectCycle() {
        boolean[] vis = new boolean[v];
        boolean[] recst = new boolean[v];

        for (int i = 0; i < v; i++) {
            if (!vis[i] && dfs(i, vis, recst)) {
                return true;
            }
        }
        return false;
    }

    private boolean dfs(int s, boolean[] vis, boolean[] recst) {
        vis[s] = true;
        recst[s] = true;

        for (Integer x : g.get(s)) {
            if (!vis[x] && dfs(x, vis, recst)) {
                return true;
            } else {
                // x is visited
                if (recst[x]) {
                    return true;
                }
            }
        }
        recst[s] = false;
        return false;
    }
}

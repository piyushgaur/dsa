public class App {
    public static void main(String[] args) throws Exception {
        Graph o = new Graph(7);

        o.addEdge(0, 1);
        o.addEdge(2, 1);
        o.addEdge(2, 3);
        o.addEdge(3, 4);
        o.addEdge(4, 5);
        o.addEdge(5, 3);

        System.out.println(o.detectCycle());
    }
}

public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        Prims o = new Prims();
        int V = 3, E = 3;
        int[][] edges = { { 0, 1, 5 }, { 1, 2, 3 }, { 0, 2, 1 } };

        System.out.println(o.spanningTree(V, E, edges));
    }
}

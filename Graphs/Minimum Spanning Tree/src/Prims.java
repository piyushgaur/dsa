import java.util.ArrayList;
import java.util.PriorityQueue;

public class Prims {
    public int spanningTree(int V, int E, int edges[][]) {
        int res = 0, u, v, d;
        boolean[] vis = new boolean[V];
        ArrayList<ArrayList<Pair>> g = new ArrayList<>();
        PriorityQueue<Pair> pq = new PriorityQueue<>((a, b) -> (a.distance - b.distance));

        // adj matrix to adj list
        for (int i = 0; i < V; i++) {
            // initialization
            g.add(new ArrayList<>());
        }

        for (int i = 0; i < E; i++) {
            u = edges[i][0];
            v = edges[i][1];
            d = edges[i][2];

            // since mst is for bidirectional graphs.
            g.get(u).add(new Pair(v, d));
            g.get(v).add(new Pair(u, d));
        }

        pq.offer(new Pair(0, 0)); // v=0 and dist =0

        while (!pq.isEmpty()) {
            Pair p = pq.poll();
            v = p.node;
            d = p.distance;

            if (!vis[v]) {

                vis[v] = true;
                res += d;

                for (Pair xPair : g.get(v)) {
                    if (!vis[xPair.node]) {
                        pq.offer(new Pair(xPair.node, xPair.distance));
                    }
                }
            }
        }

        return res;
    }
}

public class App {
    public static void main(String[] args) throws Exception {
        Graph o = new Graph(6);
        o.addEdge(0, 1);
        o.addEdge(2, 1);
        o.addEdge(2, 3);
        o.addEdge(1, 3);
        o.addEdge(2, 4);
        o.addEdge(5, 4);

        // o.displayGraph();
        System.out.println(o.detectCycle());
    }
}

import java.util.ArrayList;

/**
 * Graph
 */
public class Graph {
    ArrayList<ArrayList<Integer>> g;
    int V;

    Graph(int v) {
        V = v;
        g = new ArrayList<>();

        // initialize
        for (int i = 0; i < v; i++) {
            g.add(new ArrayList<>());
        }
    }

    public void addEdge(int s, int d) {
        g.get(s).add(d);
        g.get(d).add(s);
    }

    public void displayGraph() {
        for (ArrayList<Integer> list : g) {
            for (Integer x : list) {
                System.out.print(x + ", ");
            }
            System.out.println();
        }
    }

    public boolean detectCycle() {
        boolean[] vis = new boolean[V];

        for (int i = 0; i < V; i++) {
            if (!vis[i]) {
                if (dfs(i, -1, vis)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean dfs(int s, int parent, boolean[] vis) {
        vis[s] = true;

        for (Integer x : g.get(s)) {
            if (!vis[x]) {
                // visited false
                if (dfs(x, s, vis)) {
                    return true;
                }
            } else {
                // visited true.
                if (parent != x) {
                    return true;
                }
            }
        }
        return false;
    }
}
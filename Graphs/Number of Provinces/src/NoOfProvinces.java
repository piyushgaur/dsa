import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class NoOfProvinces {
    /**
     * Number of Provinces
     * MediumAccuracy: 54.29%Submissions: 26649Points: 4
     * Given an undirected graph with V vertices. We say two vertices u and v belong
     * to a single province if there is a path from u to v or v to u. Your task is
     * to find the number of provinces.
     * 
     * Note: A province is a group of directly or indirectly connected cities and no
     * other cities outside of the group.
     * 
     * Example 1:
     * 
     * Input:
     * [
     * [1, 0, 1],
     * [0, 1, 0],
     * [1, 0, 1]
     * ]
     * 
     * Output:
     * 2
     * Explanation:
     * The graph clearly has 2 Provinces [1,3] and [2]. As city 1 and city 3 has a
     * path between them they belong to a single province. City 2 has no path to
     * city 1 or city 3 hence it belongs to another province.
     * Example 2:
     * Input:
     * [
     * [1, 1],
     * [1, 1]
     * ]
     * 
     * Output :
     * 1
     * 
     * Your Task:
     * You don't need to read input or print anything. Your task is to complete the
     * function numProvinces() which takes an integer V and an adjacency matrix adj
     * as input and returns the number of provinces. adj[i][j] = 1, if nodes i and j
     * are connected and adj[i][j] = 0, if not connected.
     * 
     * 
     * Expected Time Complexity: O(V2)
     * Expected Auxiliary Space: O(V)
     * 
     * 
     * Constraints:
     * 1 ≤ V ≤ 500
     * 
     * @param adj adjacency list
     * @param V   no. of vertices
     * @return the no. of disconnected components
     */
    public static int numProvinces(ArrayList<ArrayList<Integer>> adj, int V) {
        int cnt = 0;
        ArrayList<ArrayList<Integer>> graph = new ArrayList<ArrayList<Integer>>();
        boolean[] vis = new boolean[V + 1];

        for (int i = 0; i < V; i++) {
            graph.add(new ArrayList<Integer>()); // intialization
        }

        for (int i = 0; i < adj.size(); i++) {
            for (int j = 0; j < adj.get(i).size(); j++) {
                if (i != j && adj.get(i).get(j) == 1) {
                    graph.get(i).add(j);
                    graph.get(j).add(i);
                }
            }
        }

        for (int i = 0; i < V; i++) {
            if (vis[i] == false) {
                dfs(vis, i, graph);
                cnt++;
            }
        }
        return cnt;
    }

    public static void dfs(boolean[] vis, int s, ArrayList<ArrayList<Integer>> graph) {
        vis[s] = true;

        for (Integer x : graph.get(s)) {
            if (vis[x] == false) {
                dfs(vis, x, graph);
            }
        }
    }

    public static void bfs(boolean[] vis, int s, ArrayList<ArrayList<Integer>> graph) {
        Queue<Integer> q = new LinkedList<>();
        q.offer(s);
        vis[s] = true;

        while (!q.isEmpty()) {
            int u = q.poll();

            for (Integer x : graph.get(u)) {
                if (vis[x] == false) {
                    q.offer(x);
                    vis[x] = true;
                }
            }
        }
    }

}

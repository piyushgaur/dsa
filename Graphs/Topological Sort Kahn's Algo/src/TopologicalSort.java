import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class TopologicalSort {

    /**
     * Given a Directed Acyclic Graph (DAG) with V vertices and E edges, Find any
     * Topological Sorting of that Graph.
     * 
     * @param V   size of vertices
     * @param adj graph, adjacency list
     * @return result array
     */
    public int[] topoSort(int V, ArrayList<ArrayList<Integer>> adj) {
        int[] res = new int[V];
        int[] indg = new int[V];
        Queue<Integer> q = new LinkedList<>();
        int k = 0, u;

        // build indegree
        for (ArrayList<Integer> l : adj) {
            for (Integer m : l) {
                indg[m]++;
            }
        }

        // add vertices whose indegree is 0
        for (int i = 0; i < V; i++) {
            if (indg[i] == 0) {
                q.offer(i);
            }
        }

        // take out adjacents of vertex and decrease the indegree, if
        // it becomes 0, add to the q

        while (!q.isEmpty()) {
            u = q.poll();
            res[k++] = u;

            for (Integer v : adj.get(u)) {
                indg[v]--;
                if (indg[v] == 0) {
                    q.offer(v);
                }
            }
        }

        return res;
    }
}

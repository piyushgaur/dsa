import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class DetectCycleDirected {
    /**
     * Given a Directed Graph with V vertices (Numbered from 0 to V-1) and E edges,
     * check whether it contains any cycle or not.
     * 
     * @param V
     * @param adj
     * @return
     */
    public boolean isCyclic(int V, ArrayList<ArrayList<Integer>> adj) {
        Queue<Integer> q = new LinkedList<>();
        int[] indg = new int[V];
        int cnt = 0, u;

        // set indegree
        for (ArrayList<Integer> l : adj) {
            for (Integer i : l) {
                indg[i]++;
            }
        }

        // push in queue which has 0 indegree
        for (int i = 0; i < indg.length; i++) {
            if (indg[i] == 0) {
                q.offer(i);
            }
        }

        // remove from q and bfs
        while (!q.isEmpty()) {
            u = q.poll();
            cnt++;

            for (Integer v : adj.get(u)) {
                indg[v]--;
                if (indg[v] == 0) {
                    q.offer(v);
                }
            }
        }
        return (cnt != V);
    }
}

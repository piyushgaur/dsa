import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<ArrayList<Integer>> g = new ArrayList<>();
        int V = 5;
        int[] res = new int[V];
        DetectCycleDirected o = new DetectCycleDirected();

        // initialize
        for (int i = 0; i < V; i++) {
            g.add(new ArrayList<>());
        }
        // input
        g.get(4).add(4);
        g.get(0).add(1);
        g.get(1).add(2);
        g.get(2).add(3);
        g.get(3).add(3);

        System.out.println(o.isCyclic(V, g));
    }
}

public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        ShortestPath o = new ShortestPath(4);
        o.addEdge(0, 1);
        o.addEdge(1, 2);
        o.addEdge(2, 3);
        o.addEdge(0, 2);
        o.addEdge(1, 3);

        // o.display();
        o.bfsUtil();
    }
}

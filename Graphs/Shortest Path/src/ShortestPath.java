import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class ShortestPath {
    Queue<Integer> q;
    boolean[] vis;
    int v;
    ArrayList<ArrayList<Integer>> g;
    int[] dist;

    ShortestPath(int v) {
        this.v = v;
        vis = new boolean[v];
        g = new ArrayList<>(v);
        q = new LinkedList<>();
        dist = new int[v];

        // intialization
        for (int i = 0; i < v; i++) {
            g.add(new ArrayList<>());
            dist[i] = Integer.MAX_VALUE;
        }
    }

    public void addEdge(int s, int d) {
        // undirected
        g.get(s).add(d);
        g.get(d).add(s);
    }

    public void display() {
        int i = 0;
        for (ArrayList<Integer> list : g) {
            System.out.print(i++ + "--> ");
            for (Integer x : list) {
                System.out.print(x + ", ");
            }
            System.out.println();
        }
    }

    public void bfsUtil() {
        bfs(0, q, vis, dist);
        System.out.println("dist array: ");
        for (int i = 0; i < v; i++) {
            System.out.println(i + " -> " + dist[i]);
        }
    }

    private void bfs(int s, Queue<Integer> q, boolean[] vis, int[] dist) {
        q.offer(s);
        vis[s] = true;
        dist[s] = 0;

        while (!q.isEmpty()) {
            int u = q.poll();

            for (Integer v : g.get(u)) {
                if (!vis[v]) {
                    vis[v] = true;
                    q.offer(v);
                    dist[v] = dist[u] + 1;
                }
            }
        }
    }
}

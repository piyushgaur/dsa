public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        Graph o = new Graph();
        // 0
        o.addEdge(0, 1);
        o.addEdge(0, 2);
        // 1
        o.addEdge(1, 2);
        o.addEdge(1, 3);
        // 2
        o.addEdge(2, 3);
        o.addEdge(2, 4);
        // 3
        o.addEdge(3, 4);
        // 5
        o.addEdge(5, 6);
        o.addEdge(5, 7);
        // 6
        o.addEdge(6, 8);

        o.displayGraph();
        // o.displayGraph2();
        System.out.println("disconnected dfs");
        o.dfsUtil();
        System.out.println("disconnected bfs");
        o.bfsUtil();
        // o.bfsUtil(0);
    }
}

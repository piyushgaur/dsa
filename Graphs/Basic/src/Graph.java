import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

// import java.util.ArrayList;

// public class Graph {
//     ArrayList<ArrayList<Integer>> g;
//     int v;

//     Graph() {
//         v = 5;
//         g = new ArrayList<ArrayList<Integer>>(v);

//         for (int i = 0; i < v; i++) {
//             g.add(new ArrayList<Integer>());
//         }
//     }

//     public void addEdge(int s, int d) {
//         // it is an undirected graph
//         // therefore
//         g.get(s).add(d);
//         g.get(d).add(s);
//     }

//     public void displayGraph() {
//         for (ArrayList<Integer> aList : g) {
//             for (Integer x : aList) {
//                 System.out.print(x + " --> ");
//             }
//             System.out.println();
//         }
//     }

//     public void displayGraph2() {
//         for (int i = 0; i < g.size(); i++) {
//             System.out.print(i + " --> ");
//             for (int j = 0; j < g.get(i).size(); j++) {
//                 System.out.print(g.get(i).get(j) + " ");
//             }
//             System.out.println();
//         }
//     }
// }

/**
 * Graph
 */
public class Graph {
    ArrayList<ArrayList<Integer>> g;
    int v, i;
    boolean[] vis;

    Graph() {
        v = 9;
        i = 0;
        g = new ArrayList<ArrayList<Integer>>(v);
        vis = new boolean[v];

        for (int i = 0; i < v; i++) {
            g.add(new ArrayList<Integer>());
        } // intialized
    }

    public void addEdge(int s, int d) {
        // undirected graph
        g.get(s).add(d);
        g.get(d).add(s);
    }

    public void displayGraph() {
        for (ArrayList<Integer> al : g) {
            System.out.print(i++ + " ==> ");
            for (Integer x : al) {
                System.out.print(x + ", ");
            }
            System.out.println();
        }
    }

    public void displayGraph2() {
        for (int i = 0; i < g.size(); i++) {
            System.out.print(i + " ==> ");
            for (int j = 0; j < g.get(i).size(); j++) {
                System.out.print(g.get(i).get(j) + ", ");
            }
            System.out.println();
        }
    }

    public void bfs(int s) {
        Queue<Integer> q = new LinkedList<>();

        q.offer(s);
        vis[s] = true;

        while (!q.isEmpty()) {
            int u = q.poll();
            System.out.print(u + ", ");
            /**
             * remove from q
             * print the element
             */

            for (Integer x : g.get(u)) {
                if (vis[x] == false) {
                    vis[x] = true;
                    q.offer(x);
                    /**
                     * make visited true
                     * add element to q
                     */
                }
            }
        }
    }

    public void bfsUtil() {
        boolean[] vis = new boolean[v + 1];
        Queue<Integer> q = new LinkedList<>();

        for (int i = 0; i < v; i++) {
            if (vis[i] == false) {
                bfsDis(i, vis, q);
            }
        }
    }

    private void bfsDis(int s, boolean[] vis, Queue<Integer> q) {
        q.offer(s);
        vis[s] = true;

        while (!q.isEmpty()) {
            int u = q.poll();
            System.out.print(u + ", ");

            for (Integer x : g.get(u)) {
                if (vis[x] == false) {
                    vis[x] = true;
                    q.offer(x);
                }
            }
        }
    }

    private void dfs(boolean[] vis, int s) {
        vis[s] = true;
        System.out.print(s + ", ");

        for (Integer x : g.get(s)) {
            if (vis[x] == false) {
                dfs(vis, x);
            }
        }
    }

    public void dfsUtil() {
        boolean[] vis = new boolean[v + 1];
        // dfs(vis, 5);
        for (int i = 0; i < v; i++) {
            if (vis[i] == false) {

                dfs(vis, i);
            }
        }
    }
}
import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<ArrayList<Integer>> g = new ArrayList<>();
        int V = 4;
        int[] res = new int[V];
        TopologicalSort o = new TopologicalSort();

        // initialize
        for (int i = 0; i < V; i++) {
            g.add(new ArrayList<>());
        }
        // input
        g.get(3).add(4);
        g.get(3).add(0);
        g.get(1).add(0);
        g.get(2).add(0);

        res = o.topoSort(V, g);
    }
}

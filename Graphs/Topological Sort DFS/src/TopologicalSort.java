import java.util.ArrayList;
import java.util.Stack;

public class TopologicalSort {
    public int[] topoSort(int V, ArrayList<ArrayList<Integer>> adj) {
        int i = 0;
        int[] res = new int[V];
        Stack<Integer> st = new Stack<>();
        boolean[] vis = new boolean[V];

        for (int j = 0; j < V; j++) {
            if (!vis[j]) {
                dfs(V, adj, j, vis, st);
            }
        }

        while (!st.isEmpty()) {
            res[i++] = st.pop();
        }
        return res;
    }

    public void dfs(int V, ArrayList<ArrayList<Integer>> adj, int u, boolean[] vis, Stack<Integer> st) {
        vis[u] = true;

        for (Integer v : adj.get(u)) {
            if (!vis[v]) {
                dfs(V, adj, v, vis, st);
            }
        }
        st.push(u);
    }
}
